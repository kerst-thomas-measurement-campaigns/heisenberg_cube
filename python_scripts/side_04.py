# constants
PATH_BACKGROUND = "/side_04/2019-08-09_09-11/2019-08-09_09-11.jpg"
PATH_COUNTS = "/side_04/2019-08-09_09-11/2019-08-09_09-11.csv"
PATH_OVERLAY = "/side_04/overlay_python.jpg"
PATH_WRITE_PROFILE = "/side_04/profile_"
OFFSET_ROWS = -30
THRESHOLD_UV = .2
CROP_COLS = [350, 690]
CROP_ROWS = [150, 450]
PROFILE_COL = 490
PROFILE_ROW = 310


# globals
background = None
uv_map = None
overlay = None


# classes
class Counts():
	def __init__(self, x_rad, y_rad, x_img, y_img, cps):
		super(Counts, self).__init__()
		self.x_rad = x_rad
		self.y_rad = y_rad
		self.x_img = x_img
		self.y_img = y_img
		self.cps = cps

		return

	def calib_x(self, a, b):
		self.col = int(round(a * self.x_rad + b))

		return

	def calib_y(self, a, b):
		self.row = int(round(a * self.y_rad + b + OFFSET_ROWS))

		return


# functions
def load_background():
	import os
	import cv2  # install with: pip3 install opencv-python
	global background

	path_dir = os.path.dirname(os.path.realpath(__file__))
	path_full = '/'.join(path_dir.split('/')[:-1]) + PATH_BACKGROUND
	background = cv2.imread(path_full)  # load as uint8 with (rows, cols, b/g/r)

	return

def get_raw_counts():
	import os

	# read csv file line by line
	path_dir = os.path.dirname(os.path.realpath(__file__))
	path_full = '/'.join(path_dir.split('/')[:-1]) + PATH_COUNTS
	counts = []

	f = open(path_full)
	for line in f:
		line_split = line.split(',')
		if line_split.__len__() < 7:  # discard lines that do not countain counts
			continue

		if line_split[-1] == 'counts\n':  # discard header line
			continue

		x_rad = int(line_split[0])
		y_rad = int(line_split[1])
		x_img = int(line_split[4])
		y_img = int(line_split[3])
		cps = float(line_split[-1])
		counts.append(Counts(x_rad, y_rad, x_img, y_img, cps))

	f.close()

	return counts

def linear_fit(data):
	import numpy as np

	x = [_d[0] for _d in data]
	y = [_d[1] for _d in data]

	# means
	xm = np.mean(x)
	ym = np.mean(y)

	# covariances
	sxy = sum([(i - xm) * (j - ym) for i, j in zip(x, y)])
	sxx = sum([(i - xm) ** 2 for i in x])

	# fit parameters
	a = sxy / sxx
	b = ym - a * xm

	return a, b

def build_uv_map(counts):
	import numpy as np
	global uv_map

	# calibrate
	# -x / colums
	x_rads = np.sort(np.unique([_c.x_rad for _c in counts]))[1:-1]
	x_fit_data = [(_c.x_rad, _c.x_img) for _c in counts if _c.x_rad in x_rads]
	a, b = linear_fit(x_fit_data)
	colum_spacing = abs(int(a / 2)) + 1
	[_c.calib_x(a, b) for _c in counts]

	# -y / rows
	y_rads = np.sort(np.unique([_c.y_rad for _c in counts]))[1:-1]
	y_fit_data = [(_c.y_rad, _c.y_img) for _c in counts if _c.y_rad in y_rads]
	a, b = linear_fit(y_fit_data)
	row_spaing = abs(int(a / 2)) + 1
	[_c.calib_y(a, b) for _c in counts]

	# intiate uv_map
	uv_map = np.zeros(shape=[*background.shape[:-1], 1], dtype=np.float32)

	# add count information
	for _c in counts:
		uv_map[_c.row - row_spaing:_c.row + row_spaing, _c.col - colum_spacing:_c.col + colum_spacing] = _c.cps

	# normalise
	uv_map = np.divide(uv_map, np.max(uv_map))

	return

def hsv_to_bgr(h, s, v):
	chroma = v * s
	hue_prime = h / 60
	x = chroma * (1 - abs(hue_prime % 2 - 1))

	rgb = [0, 0, 0]
	if 0 <= hue_prime <= 1:
		rgb = [chroma, x, 0]

	if 1 < hue_prime <= 2:
		rgb = [x, chroma, 0]

	if 2 < hue_prime <= 3:
		rgb = [0, chroma, x]

	if 3 < hue_prime <= 4:
		rgb = [0, x, chroma]

	if 4 < hue_prime <= 5:
		rgb = [x, 0, chroma]

	if 5 < hue_prime <= 6:
		rgb = [chroma, 0, x]

	m = v - chroma
	rgb = [rgb[0] + m, rgb[1] + m, rgb[2] + m]

	return [rgb[2] * 255, rgb[1] * 255, rgb[0] * 255]

def compile_overlay():
	import cv2
	import numpy as np
	global overlay

	# intitate the overlay
	overlay = np.zeros(shape=background.shape, dtype=np.uint8)

	# iterate through every pixel and make a hsv overlay
	for row in range(overlay.shape[0]):
		for col in range(overlay.shape[1]):
			if uv_map[row, col] < THRESHOLD_UV:
				overlay[row, col] = background[row, col]

				continue

			hue = 270 - 270 * uv_map[row, col]
			# saturation = (uv_map[row, col] - THRESHOLD_UV) / (1.0 - THRESHOLD_UV)
			saturation = 1
			value = background[row, col][0] / 256
			overlay[row, col] = hsv_to_bgr(hue, saturation, value)

	# crop the overlay
	overlay = overlay[CROP_ROWS[0]: CROP_ROWS[1], CROP_COLS[0]: CROP_COLS[1]]

	return

def save_profile(**kwargs):
	import os
	import matplotlib.pyplot as plt

	# path
	path_dir = os.path.dirname(os.path.realpath(__file__))
	path_full = '/'.join(path_dir.split('/')[:-1]) + PATH_WRITE_PROFILE + kwargs['name'] + '.png'

	# data
	y = kwargs['profile']
	x = [*range(y.__len__())]

	# write
	plt.plot(x, y, color='0')
	plt.xlabel('Pixel number')
	plt.ylabel('UV Intensity ' + kwargs['ylab'] + ' (normalised)')
	plt.savefig(path_full)
	plt.close()

	return

def extract_profiles():
	global overlay

	# col
	profile = uv_map[:, PROFILE_COL]
	overlay[:, PROFILE_COL - CROP_COLS[0]] = [41, 244, 234]
	save_profile(profile=profile, name="col", ylab="Columns")

	# row
	profile = uv_map[PROFILE_ROW, :]
	overlay[PROFILE_ROW - CROP_ROWS[0], :] = [41, 244, 234]
	save_profile(profile=profile, name="row", ylab="Rows")

	return

def save_overlay():
	import os
	import cv2

	path_dir = os.path.dirname(os.path.realpath(__file__))
	path_full = '/'.join(path_dir.split('/')[:-1]) + PATH_OVERLAY

	cv2.imwrite(path_full, overlay)

	return


# main
def main():
	load_background()
	build_uv_map(get_raw_counts())
	compile_overlay()
	extract_profiles()
	save_overlay()

	return


if __name__ == '__main__':
	main()
