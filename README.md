# Heisenberg Cube Measurement Campaign in Tampere


## Time and Place

| Parameter | Value |
| - | - |
| Location | Tampere, Finland |
| Start | 2019-08-07 |
| End | 2019-08-11 |


## Flow Chamber Information

### Box

| Parameter | Value |
| - | - |
| Inner Dimensions (mm) | 230 x 230 x 230 |
| Thickness (mm) | 10 |
| Volume (L) | 12.17 |
| Material | Acryl |
| Supplier | [Tunkua](https://tunkua.fi/) |


### Window

| Parameter | Value |
| - | - |
| Diameter (mm) | 150 |
| Thickness (mm) | 6 |
| Material | UV Fused Silica |
| Supplier | [UQG Optics](https://www.uqgoptics.com) |
| Product Code | [WFS-1506](https://www.uqgoptics.com/product/uv-fused-silica-window-9/) |


### Gas

| Parameter | Value |
| - | - |
| NO content (ppm) | 3 |
| Nitrogen Purity | 5.0 |
| Total Mass Flow (SLPM) | 5 |
| Gas Supplier | [AGA](https://www.aga.fi/fi/index.html) |


### Mass Flow Controllers

| Parameter | Value |
| - | - |
| Supplier | [Bronkhorst](https://www.bronkhorst.com/int/) |
| Product Code (NO control) | [18BRF-201CV-1K0-AAD-22-V](https://www.bronkhorst.com/int/products/gas-flow/el-flow-select/f-201cv/) |
| Product Code (N2 control) | [18BRF-201CV-10K-AAD-22-V]((https://www.bronkhorst.com/int/products/gas-flow/el-flow-select/f-201cv/)) |


## Scanner Information

### PMT

| Parameter | Value |
| - | - |
| Spectral Response (nm) | 185 - 320 |
| Supplier | [Hamamatsu](https://www.hamamatsu.com/us/en/index.html) |
| Model | [H11870–09](https://www.hamamatsu.com/resources/pdf/etd/H11870_TPMO1061E.pdf) |


### Filters

| Parameter | Value |
| - | - |
| Quantity | 3 |
| Spectral Response (nm) | 252 - 268 |
| Supplier | [Semrock](https://www.semrock.com/) |
| Model | [FF01–260/16–25](https://www.semrock.com/FilterDetails.aspx?id=FF01-260/16-25) |


### Optics

| Parameter | Value |
| - | - |
| Field of View (deg) | 3.4 |
| Design | as [described](https://trepo.tuni.fi/handle/10024/114881) by Johan Sand |


### Webcam

| Parameter | Value |
| - | - |
| Field of View (deg) | 90 |
| Resolution (px) | 1920 x 1080 |
| Supplier | [Logitech](https://www.logitech.com/en-us) |
| Model | [C930E](https://www.logitech.com/en-us/product/c930e-webcam#) |
